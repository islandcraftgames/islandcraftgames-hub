package net.islandcraftgames.minigames.listeners;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;

import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.api.minigames.SignManager.GameSign;
import net.islandcraftgames.api.minigames.events.GamePlayerDamageEvent;
import net.islandcraftgames.api.minigames.events.GamePlayerDeathEvent;
import net.islandcraftgames.api.minigames.events.GamePlayerInteractEvent;
import net.islandcraftgames.hub.Main;
import net.islandcraftgames.hub.player.IslandPlayer;
import net.islandcraftgames.hub.player.PlayerData;

public class PlayerListener implements Listener {

	@EventHandler
	public void onSignClick(PlayerInteractEvent e) {
		if (e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return;
		Block b = e.getClickedBlock();
		GameSign gs = Main.get().getSignManager().getByLocation(b.getLocation());
		if (gs == null)
			return;
		Game game = Main.get().getGameManager().getGame(gs.getType(), gs.getMap());
		game.addPlayer(PlayerData.getData(e.getPlayer()));
	}

	/* Event triggers */

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		IslandPlayer ip = PlayerData.getData(e.getPlayer());
		if (!ip.isPlaying())
			return;
		GamePlayerInteractEvent ie = new GamePlayerInteractEvent(ip.getGame(), ip, e.getAction(), e.getItem(),
				e.getClickedBlock(), e.getBlockFace());
		Bukkit.getPluginManager().callEvent(ie);
		e.setCancelled(ie.isCancelled());
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onLastDamage(EntityDamageEvent e) {
		if (!(e.getEntity() instanceof Player))
			return;
		Player p = (Player) e.getEntity();
		IslandPlayer ip = PlayerData.getData(p);
		if (ip.isPlaying())
			if (p.getHealth() - e.getFinalDamage() <= 0) {
				e.setCancelled(true);
				GamePlayerDeathEvent de = new GamePlayerDeathEvent(ip.getGame(), ip);
				Bukkit.getPluginManager().callEvent(de);
				if (de.getRespawnLocation() != null)
					p.teleport(de.getRespawnLocation());
			}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerDamage(EntityDamageEvent e) {
		if (e.getCause() == DamageCause.ENTITY_ATTACK)
			return;
		if (!(e.getEntity() instanceof Player))
			return;
		Player p = (Player) e.getEntity();
		IslandPlayer ip = PlayerData.getData(p);
		if (!ip.isPlaying())
			return;
		GamePlayerDamageEvent ev = new GamePlayerDamageEvent(ip.getGame(), ip, e.getCause(), e.getDamage(), null);
		Bukkit.getPluginManager().callEvent(ev);
		e.setCancelled(ev.isCancelled());
		e.setDamage(ev.getDamage());
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerDamageByEntity(EntityDamageByEntityEvent e) {
		if (!(e.getEntity() instanceof Player))
			return;
		Player p = (Player) e.getEntity();
		IslandPlayer ip = PlayerData.getData(p);
		if (!ip.isPlaying())
			return;
		GamePlayerDamageEvent ev = new GamePlayerDamageEvent(ip.getGame(), ip, e.getCause(), e.getDamage(),
				e.getDamager());
		Bukkit.getPluginManager().callEvent(ev);
		e.setCancelled(ev.isCancelled());
		e.setDamage(ev.getDamage());
		if (!ev.isCancelled() && ev.getDamager() instanceof Player)
			ev.getGame().setLastHit(ev.getGamePlayer(), PlayerData.getData((Player) ev.getDamager()));
		if (!ev.isCancelled() && ev.getDamager() instanceof Projectile
				&& ((Projectile) ev.getDamager()).getShooter() instanceof Player)
			ev.getGame().setLastHit(ev.getGamePlayer(),
					PlayerData.getData((Player) ((Projectile) ev.getDamager()).getShooter()));
		if (!ev.isCancelled() && ev.getDamager() instanceof TNTPrimed
				&& ((TNTPrimed) ev.getDamager()).getSource() instanceof Player)
			ev.getGame().setLastHit(ev.getGamePlayer(),
					PlayerData.getData((Player) ((TNTPrimed) ev.getDamager()).getSource()));
		if (!ev.isCancelled() && ev.getDamager() instanceof TNTPrimed
				&& ((TNTPrimed) ev.getDamager()).getSource() instanceof Projectile
				&& ((Projectile) ((TNTPrimed) ev.getDamager()).getSource()).getShooter() instanceof Player)
			ev.getGame().setLastHit(ev.getGamePlayer(),
					PlayerData.getData((Player) ((Projectile) ((TNTPrimed) ev.getDamager()).getSource()).getShooter()));
	}

}
