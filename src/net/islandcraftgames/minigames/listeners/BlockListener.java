package net.islandcraftgames.minigames.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;

import net.islandcraftgames.api.minigames.GameType;
import net.islandcraftgames.api.minigames.SignManager.GameSign;
import net.islandcraftgames.hub.Main;

public class BlockListener implements Listener {

	@EventHandler
	public void onSignPlace(SignChangeEvent e) {
		if (!(e.getLine(0).length() != 0 || e.getLine(1).length() != 0 || e.getLine(2).length() == 0
				|| e.getLine(3).length() == 0))
			return;
		String name = e.getLine(0);
		if (!(name.startsWith("[") && name.endsWith("]")))
			return;
		name = name.substring(1, name.length() - 1);
		GameType gt = GameType.getFromAlias(name);
		if (gt == null)
			return;
		e.setLine(0, ChatColor.AQUA + "[" + gt.getFullName() + "]");
		Main.get().getSignManager().addSign(gt, e.getBlock().getLocation(), e.getLine(1));
	}

	@EventHandler
	public void onSignBreak(BlockBreakEvent e) {
		GameSign gs = Main.get().getSignManager().getByLocation(e.getBlock().getLocation());
		if (gs == null)
			return;
		gs.delete();
	}

}
