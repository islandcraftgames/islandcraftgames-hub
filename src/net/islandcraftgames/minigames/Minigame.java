package net.islandcraftgames.minigames;

import java.io.File;

import net.islandcraftgames.api.minigames.GameType;
import net.islandcraftgames.api.minigames.Kit;
import net.islandcraftgames.hub.Main;

public abstract class Minigame {

	public abstract GameType getGameType();

	public String getGameFullName() {
		return getGameType().getFullName();
	}

	public String getGameNameAlias() {
		return getGameType().getAlias();
	}

	public void addKit(Kit kit) {
		Main.get().getKitManager().addKit(getGameType(), kit);
	}

	public abstract File getDataFolder();

}
