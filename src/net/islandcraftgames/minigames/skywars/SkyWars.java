package net.islandcraftgames.minigames.skywars;

import java.io.File;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.islandcraftgames.api.minigames.GameType;
import net.islandcraftgames.api.minigames.Kit;
import net.islandcraftgames.hub.Main;
import net.islandcraftgames.minigames.Minigame;

public class SkyWars extends Minigame {

	private File dataFolder;
	private File schematicsFolder;

	public SkyWars() {
		Bukkit.getPluginManager().registerEvents(new SkyWarsListener(), Main.get());
		dataFolder = new File(Main.get().getDataFolder(), "SkyWars");
		if (!dataFolder.exists())
			dataFolder.mkdirs();
		schematicsFolder = new File(dataFolder, "schematics");
		if (!schematicsFolder.exists())
			schematicsFolder.mkdirs();
		Main.get().getSchematicManager().searchFolder(schematicsFolder, getGameType());
		setupKits();
	}

	private void setupKits() {
		Main.get().getKitManager().setShopRows(getGameType(), 5);
		addKit(new Kit(getGameType(), 0, "sw.kit0", 1000, -1, 10, "", new ItemStack(Material.SNOW_BALL, 5),
				new ItemStack(Material.SNOW_BALL, 5)));
		addKit(new Kit(getGameType(), 1, "sw.kit1", 2000, 0, 19, "", new ItemStack(Material.SNOW_BALL, 10),
				new ItemStack(Material.SNOW_BALL, 5)));
		addKit(new Kit(getGameType(), 2, "sw.kit2", 3000, 1, 28, "", new ItemStack(Material.SNOW_BALL, 16),
				new ItemStack(Material.SNOW_BALL, 5)));
		addKit(new Kit(getGameType(), 3, "sw.kit3", 1500, -1, 13, "", new ItemStack(Material.TNT),
				new ItemStack(Material.TNT, 10), new ItemStack(Material.FLINT_AND_STEEL)));
		addKit(new Kit(getGameType(), 4, "sw.kit4", 500, -1, 21, "", new ItemStack(Material.FISHING_ROD),
				new ItemStack(Material.FISHING_ROD)));
		addKit(new Kit(getGameType(), 5, "sw.kit5", 1500, -1, 22, "", new ItemStack(Material.BOW),
				new ItemStack(Material.BOW), new ItemStack(Material.ARROW, 7)));
		addKit(new Kit(getGameType(), 6, "sw.kit6", 3000, -1, 31, "", new ItemStack(Material.GOLDEN_APPLE, 3),
				new ItemStack(Material.GOLDEN_APPLE, 3)) {
			@Override
			public ItemStack[] getItems(Player p) {
				Random r = new Random();
				int chance = 10;
				if (p.hasPermission("vip.plus.plus"))
					chance = 70;
				else if (p.hasPermission("vip.plus"))
					chance = 50;
				else if (p.hasPermission("vip"))
					chance = 20;
				if (r.nextInt(100) < chance)
					return items;
				return new ItemStack[] { new ItemStack(Material.AIR) };
			}
		});
		addKit(new Kit(getGameType(), 7, "sw.kit7", 1250, -1, 23, "", new ItemStack(Material.IRON_PICKAXE),
				new ItemStack(Material.IRON_PICKAXE)));
		addKit(new Kit(getGameType(), 8, "sw.kit8", 2000, -1, 16, "", new ItemStack(Material.ENDER_PEARL),
				new ItemStack(Material.ENDER_PEARL)));
		addKit(new Kit(getGameType(), 9, "sw.kit9", 4000, 8, 25, "", new ItemStack(Material.ENDER_PEARL, 2),
				new ItemStack(Material.ENDER_PEARL)));
		addKit(new Kit(getGameType(), 10, "sw.kit10", 6000, 9, 34, "", new ItemStack(Material.ENDER_PEARL, 3),
				new ItemStack(Material.ENDER_PEARL)));
	}

	@Override
	public GameType getGameType() {
		return GameType.SKYWARS;
	}

	@Override
	public File getDataFolder() {
		return dataFolder;
	}

}
