package net.islandcraftgames.minigames.skywars;

import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.google.common.collect.Lists;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.Vector;

import net.islandcraftgames.api.LocationUtils;
import net.islandcraftgames.api.MathUtils;
import net.islandcraftgames.api.language.LanguageMain;
import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.api.minigames.GameState;
import net.islandcraftgames.api.minigames.GameType;
import net.islandcraftgames.api.minigames.events.GameCreateEvent;
import net.islandcraftgames.api.minigames.events.GameEndEvent;
import net.islandcraftgames.api.minigames.events.GameForceStartEvent;
import net.islandcraftgames.api.minigames.events.GamePlayerDamageEvent;
import net.islandcraftgames.api.minigames.events.GamePlayerDeathEvent;
import net.islandcraftgames.api.minigames.events.GamePlayerJoinEvent;
import net.islandcraftgames.api.minigames.events.GamePlayerLeaveEvent;
import net.islandcraftgames.api.minigames.events.GameSecondEvent;
import net.islandcraftgames.api.minigames.events.GameStartEvent;
import net.islandcraftgames.api.minigames.events.SchematicScanEvent;
import net.islandcraftgames.api.minigames.events.SpectatorLeaveEvent;
import net.islandcraftgames.hub.Main;
import net.islandcraftgames.hub.player.IslandPlayer;

@SuppressWarnings("deprecation")
public class SkyWarsListener implements Listener {

	@EventHandler
	public void onSchematicScan(SchematicScanEvent e) {
		if (e.getGameType() != GameType.SKYWARS)
			return;
		CuboidClipboard c = e.getClipboard();
		if (c.getPoint(e.getBlock()).getType() == 138) {
			List<Vector> spawns = e.getGameMap().getVectorListData("spawns");
			spawns.add(e.getBlock().add(c.getOffset()));
			e.getGameMap().setData("spawns", spawns);
		} else if (c.getPoint(e.getBlock()).getType() == 54 || c.getPoint(e.getBlock()).getType() == 146) {
			List<Vector> chests = e.getGameMap().getVectorListData("chests");
			chests.add(e.getBlock().add(c.getOffset()));
			e.getGameMap().setData("chests", chests);
		}
	}

	@EventHandler
	public void onGameCreate(GameCreateEvent e) {
		if (e.getGameType() != GameType.SKYWARS)
			return;
		List<Vector> spawnsVec = e.getGame().getGameMap().getVectorListData("spawns");
		List<Location> spawns = Lists.newArrayList();
		for (Vector vec : spawnsVec)
			spawns.add(e.getGame().getCenter().add(vec.getX(), vec.getY(), vec.getZ()));
		e.getGame().setData("spawns", spawns);
		List<Vector> chestsVec = e.getGame().getGameMap().getVectorListData("chests");
		List<Location> chests = Lists.newArrayList();
		for (Vector vec : chestsVec)
			chests.add(e.getGame().getCenter().add(vec.getX(), vec.getY(), vec.getZ()));
		e.getGame().setData("chests", chests);
		e.getGame().setMaximumPlayers(spawns.size());
		e.getGame().setTimer(60);
	}

	@EventHandler
	public void onGameSecond(GameSecondEvent e) {
		if (e.getGameType() != GameType.SKYWARS)
			return;
		Game game = e.getGame();
		GameState gs = game.getGameState();
		switch (gs) {
		case ENDING:
			break;
		case PLAYING:
			break;
		case WAITING:
			int timer = game.getTimer();
			if (timer == 0) {
				if (game.getPlayingAmount() > 1)
					game.startGame();
				else {
					game.sendMessage("game.notenoughplayers", 2);
					game.setTimer(60);
				}
				return;
			}
			if (timer % 10 == 0 || timer <= 5) {
				for (IslandPlayer ip : game.getPlayers()) {
					ip.toBukkit().sendMessage(LanguageMain.get(ip, "game.countdown", timer,
							LanguageMain.get(ip, timer == 1 ? "time.second" : "time.seconds")));
				}
				switch (timer) {
				case 5:
				case 4:
					game.sendTitle(ChatColor.GREEN + "" + timer, "", 5, 10, 5);
					break;
				case 3:
					game.sendTitle(ChatColor.YELLOW + "" + timer, "", 5, 10, 5);
					break;
				case 2:
				case 1:
					game.sendTitle(ChatColor.RED + "" + timer, "", 5, 10, 5);
					break;
				}
			}
			game.decreaseTimer();
			break;
		default:
			break;
		}
	}

	@EventHandler
	public void onPlayerJoin(GamePlayerJoinEvent e) {
		if (e.getGameType() != GameType.SKYWARS)
			return;
		Player p = e.getGamePlayer().toBukkit();
		p.getInventory().clear();
		p.setHealthScale(20);
		p.setHealth(20);
		p.setFoodLevel(20);
	}

	@EventHandler
	public void onGameStart(GameStartEvent e) {
		if (e.getGameType() != GameType.SKYWARS)
			return;
		Game game = e.getGame();
		List<Location> chests = e.getGame().getLocationListData("chests");
		for (Location chest : chests)
			ChestManager.get().populateChest((Chest) chest.getBlock().getState());
		List<Location> spawns = e.getGame().getLocationListData("spawns");
		for (Location spawn : spawns)
			spawn.getBlock().setType(Material.AIR);
		Collections.shuffle(spawns);
		for (IslandPlayer ip : game.getPlayers()) {
			Player p = ip.toBukkit();
			p.teleport(spawns.get(0).add(0.5, 0.5, 0.5));
			p.getInventory().clear();
			p.setHealth(20);
			p.setFoodLevel(20);
			spawns.remove(0);
		}
	}

	@EventHandler
	public void onDamage(GamePlayerDamageEvent e) {
		if (e.getGameType() != GameType.SKYWARS)
			return;
		if (e.getGame().getGameState() != GameState.PLAYING)
			e.setCancelled(true);
	}

	@EventHandler
	public void onDeath(GamePlayerDeathEvent e) {
		if (e.getGameType() != GameType.SKYWARS)
			return;
		Game game = e.getGame();
		IslandPlayer ip = e.getGamePlayer();
		game.removePlayer(ip, false);
		game.addSpectator(ip);
		ip.getMinigameData(e.getGameType().getFullName()).addDeaths(1);
		if (e.getKiller() != null) {
			game.sendMessage("game.kill", ip.toBukkit().getName(), e.getKiller().toBukkit().getName());
			ip.toBukkit().setSpectatorTarget(e.getKiller().toBukkit());
			int coins = MathUtils.getPlayerCoins(e.getKiller().toBukkit(), 10);
			e.getKiller().addMoney(coins);
			e.getKiller().sendMessage("game.earncoins", coins);
			int xp = MathUtils.getPlayerCoins(e.getKiller().toBukkit(), 10);
			e.getKiller().addXp(xp);
			e.getKiller().sendMessage("game.earnxp", xp);
			e.getKiller().getMinigameData(e.getGameType().getFullName()).addKills(1);

		} else
			game.sendMessage("game.suicide", ip.toBukkit().getName());
		if (game.getPlayingAmount() != 1)
			game.sendMessage("game.playersleft", game.getPlayingAmount());
		else {
			IslandPlayer winner = game.getPlayers().get(0);
			game.sendMessage("game.won", winner.toBukkit().getName());
			winner.getMinigameData(e.getGameType().getFullName()).addWins(1);
			game.endGame();
		}
	}

	@EventHandler
	public void onGameEnd(GameEndEvent e) {
		if (e.getGameType() != GameType.SKYWARS)
			return;
		Bukkit.getScheduler().runTaskLater(Main.get(), new Runnable() {
			@Override
			public void run() {
				for (IslandPlayer ip : e.getGame().getPlayers())
					e.getGame().removePlayer(ip);
				for (IslandPlayer ip : e.getGame().getSpectators())
					e.getGame().removeSpectator(ip);
			}
		}, 100L);
	}

	@EventHandler
	public void onPlayerLeave(GamePlayerLeaveEvent e) {
		if (e.getGameType() != GameType.SKYWARS)
			return;
		e.getGamePlayer().toBukkit()
				.teleport(LocationUtils.fromString(Main.get().getConfig().getString("locations.sky")));
	}

	@EventHandler
	public void onSpectatorLeave(SpectatorLeaveEvent e) {
		if (e.getGameType() != GameType.SKYWARS)
			return;
		e.getGamePlayer().toBukkit()
				.teleport(LocationUtils.fromString(Main.get().getConfig().getString("locations.sky")));
	}

	@EventHandler
	public void onGameForceStart(GameForceStartEvent e) {
		if (e.getGame().getPlayingAmount() < 2) {
			e.setCancelled(true);
			e.setErrorMessage(LanguageMain.get(e.getGamePlayer(), "cmd.start.error.notenoughplayers", 2));
		}
	}

}