package net.islandcraftgames.minigames.skywars;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ChestManager {
	private static ChestManager chestController;

	public void populateChest(Chest chest) {
		Inventory inventory = chest.getBlockInventory();
		inventory.addItem(randomSword());
		inventory.addItem(randomHelmet());
		inventory.addItem(randomChestplate());
		inventory.addItem(randomLeggings());
		inventory.addItem(randomBoots());
		inventory.addItem(randomFood());
		inventory.addItem(randomBow());
		inventory.addItem(randomArrow());
		inventory.addItem(randomCobblestone());
		inventory.addItem(randomDirt());
		inventory.addItem(randomTNT());
		inventory.addItem(randomFlintAndSteel());
		inventory.addItem(randomBucket());
	}

	public ItemStack randomSword() {
		Random r = new Random();
		switch (r.nextInt(6)) {
		case 1:
			return new ItemStack(Material.WOOD_SWORD);
		case 2:
			return new ItemStack(Material.STONE_SWORD);
		case 3:
			return new ItemStack(Material.GOLD_SWORD);
		case 4:
			return new ItemStack(Material.IRON_SWORD);
		case 5:
			return new ItemStack(Material.DIAMOND_SWORD);
		default:
			return new ItemStack(Material.WOOD_SWORD);
		}
	}

	public ItemStack randomHelmet() {
		Random r = new Random();
		switch (r.nextInt(6)) {
		case 1:
			return new ItemStack(Material.LEATHER_HELMET);
		case 2:
			return new ItemStack(Material.CHAINMAIL_HELMET);
		case 3:
			return new ItemStack(Material.GOLD_HELMET);
		case 4:
			return new ItemStack(Material.IRON_HELMET);
		case 5:
			return new ItemStack(Material.DIAMOND_HELMET);
		default:
			return new ItemStack(Material.LEATHER_HELMET);
		}
	}

	public ItemStack randomChestplate() {
		Random r = new Random();
		switch (r.nextInt(6)) {
		case 1:
			return new ItemStack(Material.LEATHER_CHESTPLATE);
		case 2:
			return new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		case 3:
			return new ItemStack(Material.GOLD_CHESTPLATE);
		case 4:
			return new ItemStack(Material.IRON_CHESTPLATE);
		case 5:
			return new ItemStack(Material.DIAMOND_CHESTPLATE);
		default:
			return new ItemStack(Material.LEATHER_CHESTPLATE);
		}
	}

	public ItemStack randomLeggings() {
		Random r = new Random();
		switch (r.nextInt(6)) {
		case 1:
			return new ItemStack(Material.LEATHER_LEGGINGS);
		case 2:
			return new ItemStack(Material.CHAINMAIL_LEGGINGS);
		case 3:
			return new ItemStack(Material.GOLD_LEGGINGS);
		case 4:
			return new ItemStack(Material.IRON_LEGGINGS);
		case 5:
			return new ItemStack(Material.DIAMOND_LEGGINGS);
		default:
			return new ItemStack(Material.LEATHER_LEGGINGS);
		}
	}

	public ItemStack randomBoots() {
		Random r = new Random();
		switch (r.nextInt(6)) {
		case 1:
			return new ItemStack(Material.LEATHER_BOOTS);
		case 2:
			return new ItemStack(Material.CHAINMAIL_BOOTS);
		case 3:
			return new ItemStack(Material.GOLD_BOOTS);
		case 4:
			return new ItemStack(Material.IRON_BOOTS);
		case 5:
			return new ItemStack(Material.DIAMOND_BOOTS);
		default:
			return new ItemStack(Material.LEATHER_BOOTS);
		}
	}

	public ItemStack randomFood() {
		Random r = new Random();
		switch (r.nextInt(4)) {
		case 1:
			return new ItemStack(Material.GOLDEN_APPLE, 3);
		case 2:
			return new ItemStack(Material.APPLE, 5);
		case 3:
			return new ItemStack(Material.COOKED_BEEF, 5);
		default:
			return new ItemStack(Material.APPLE, 5);
		}
	}

	public ItemStack randomBow() {
		Random r = new Random();
		if (r.nextInt(2) == 1) {
			return new ItemStack(Material.BOW);
		} else {
			return new ItemStack(Material.AIR);
		}
	}

	public ItemStack randomArrow() {
		Random r = new Random();
		switch (r.nextInt(6)) {
		case 1:
			return new ItemStack(Material.ARROW, 1);
		case 2:
			return new ItemStack(Material.ARROW, 2);
		case 3:
			return new ItemStack(Material.ARROW, 3);
		case 4:
			return new ItemStack(Material.ARROW, 4);
		case 5:
			return new ItemStack(Material.ARROW, 5);
		default:
			return new ItemStack(Material.AIR);
		}
	}

	public ItemStack randomCobblestone() {
		Random r = new Random();
		switch (r.nextInt(6)) {
		case 1:
			return new ItemStack(Material.COBBLESTONE, 16);
		case 2:
			return new ItemStack(Material.COBBLESTONE, 32);
		case 3:
			return new ItemStack(Material.COBBLESTONE, 32 + 16);
		case 4:
			return new ItemStack(Material.COBBLESTONE, 64);
		default:
			return new ItemStack(Material.AIR);
		}
	}

	public ItemStack randomDirt() {
		Random r = new Random();
		switch (r.nextInt(5)) {
		case 1:
			return new ItemStack(Material.DIRT, 16);
		case 2:
			return new ItemStack(Material.DIRT, 32);
		case 3:
			return new ItemStack(Material.DIRT, 32 + 16);
		case 4:
			return new ItemStack(Material.DIRT, 64);
		default:
			return new ItemStack(Material.AIR);
		}
	}

	public ItemStack randomTNT() {
		Random r = new Random();
		switch (r.nextInt(5)) {
		case 1:
			return new ItemStack(Material.TNT, 2);
		case 2:
			return new ItemStack(Material.TNT, 4);
		case 3:
			return new ItemStack(Material.TNT, 8);
		case 4:
			return new ItemStack(Material.TNT, 16);
		default:
			return new ItemStack(Material.AIR);
		}
	}

	public ItemStack randomFlintAndSteel() {
		Random r = new Random();
		if (r.nextInt(2) == 1) {
			return new ItemStack(Material.FLINT_AND_STEEL);
		} else {
			return new ItemStack(Material.AIR);
		}
	}

	public ItemStack randomBucket() {
		Random r = new Random();
		switch (r.nextInt(2)) {
		case 1:
			return new ItemStack(Material.WATER_BUCKET, 1);
		case 2:
			return new ItemStack(Material.LAVA_BUCKET, 1);
		default:
			return new ItemStack(Material.BUCKET, 1);
		}
	}

	public static ChestManager get() {
		if (chestController == null) {
			chestController = new ChestManager();
		}
		return chestController;
	}
}
