package net.islandcraftgames.api;

public class StringUtils {

	public static boolean equalsIgnoreCase(String str1, String... str2) {
		for (String s : str2)
			if (s.equalsIgnoreCase(str1))
				return true;
		return false;
	}

}
