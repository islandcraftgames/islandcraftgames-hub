package net.islandcraftgames.api;

import java.lang.reflect.Field;

public class ReflectionUtils {
	
	public static Object getPrivateField(String fieldName, Class<?> clazz, Object object) {
		Field field;
		Object o = null;
		try {
			field = clazz.getDeclaredField(fieldName);
			field.setAccessible(true);
			o = field.get(object);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return o;
	}

}
