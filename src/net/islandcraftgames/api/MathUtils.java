package net.islandcraftgames.api;

import org.bukkit.entity.Player;

public class MathUtils {

	public static int getPlayerCoins(Player p, int coins) {
		if (p.hasPermission("vip++"))
			return coins * 3;
		if (p.hasPermission("vip+"))
			return coins * 2;
		if (p.hasPermission("vip"))
			return (int) (coins * 1.5);
		return coins;
	}

}
