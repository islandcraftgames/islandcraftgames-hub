package net.islandcraftgames.api.gui;

import org.bukkit.event.inventory.InventoryClickEvent;

public abstract class GuiButtonClickEvent {
	
	public abstract void onClick(InventoryClickEvent event);

}
