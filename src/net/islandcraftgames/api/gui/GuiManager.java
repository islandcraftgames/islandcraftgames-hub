package net.islandcraftgames.api.gui;

import java.util.HashMap;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import com.google.common.collect.Maps;

public class GuiManager implements Listener {

	private HashMap<Inventory, Gui> open = Maps.newHashMap();

	public void add(Inventory inv, Gui gui) {
		open.put(inv, gui);
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (!open.containsKey(e.getInventory()))
			return;
		Gui gui = open.get(e.getInventory());
		if (gui.isBlocked())
			e.setCancelled(true);
		if (e.getClickedInventory() == null)
			return;
		GuiButton btn = gui.getButton(e.getRawSlot());
		if (btn == null)
			return;
		btn.getEvent().onClick(e);
	}

	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		open.remove(e.getInventory());
	}

}
