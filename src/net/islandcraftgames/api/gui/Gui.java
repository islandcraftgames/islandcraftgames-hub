package net.islandcraftgames.api.gui;

import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.google.common.collect.Maps;

import net.islandcraftgames.hub.Main;

public class Gui {

	private int rows;
	private String title;
	private HashMap<Integer, GuiButton> items = Maps.newHashMap();
	private boolean blocked = true;

	public Gui(int rows, String title) {
		this.rows = rows;
		this.title = title;
	}

	public Gui(int rows) {
		this(rows, "");
	}

	public Gui(String title) {
		this(1, title);
	}

	public Gui() {
		this(1, "");
	}

	public int nextIndex() {
		for (int i = 0; i < getSize(); i++) {
			if (!items.containsKey(i))
				return i;
		}
		return -1;
	}

	public void addButton(GuiButton button) {
		int index = nextIndex();

		if (index == -1)
			throw new RuntimeException("Inventory cannot be full!");
		setButton(index, button);
	}

	public void setButton(int position, GuiButton button) {
		if (position > getSize())
			throw new IllegalArgumentException("Position cannot be bigger than the size!");
		items.put(position, button);
	}
	
	public GuiButton getButton(int position){
		return items.get(position);
	}

	public int getSize() {
		return rows * 9;
	}

	public void open(Player p) {
		Inventory inv = Bukkit.createInventory(null, getSize(), ChatColor.translateAlternateColorCodes('&', title));
		for (Entry<Integer, GuiButton> entry : items.entrySet())
			inv.setItem(entry.getKey(), entry.getValue().getItemStack());
		p.openInventory(inv);
		Main.get().getGuiManager().add(inv, this);
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
