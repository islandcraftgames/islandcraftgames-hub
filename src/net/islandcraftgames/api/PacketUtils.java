package net.islandcraftgames.api;

import java.lang.reflect.Field;

import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;

public class PacketUtils {

	public static void sendParticles(World w, EnumParticle particle, int x, int y, int z, int xd, int yd, int zd,
			int speed, int count) {
		PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, true, x, y, z, xd, yd, zd, speed,
				count, 0);
		for (Player p : w.getPlayers())
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
	}

	public static void sendActionBar(Player p, String msg) {
		IChatBaseComponent cbc = toJSON(msg);
		PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppoc);
	}

	public static void sendTitle(Player p, String title, String subtitle, int fadeIn, int stay, int fadeOut) {
		PacketPlayOutTitle titleP = new PacketPlayOutTitle(EnumTitleAction.TITLE, toJSON(title));
		PacketPlayOutTitle subtitleP = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, toJSON(subtitle));
		PacketPlayOutTitle timesP = new PacketPlayOutTitle(fadeIn, stay, fadeOut);
		for (PacketPlayOutTitle packet : new PacketPlayOutTitle[] { titleP, subtitleP, timesP }) {
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		}
	}

	public static void sendTab(Player p, String header, String footer) {
		PacketPlayOutPlayerListHeaderFooter headerfooter = new PacketPlayOutPlayerListHeaderFooter();
		try {
			Field headerF = headerfooter.getClass().getDeclaredField("a");
			Field footerF = headerfooter.getClass().getDeclaredField("b");
			headerF.setAccessible(true);
			footerF.setAccessible(true);
			headerF.set(headerfooter, toJSON(header));
			footerF.set(headerfooter, toJSON(footer));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(headerfooter);
	}

	private static IChatBaseComponent toJSON(String text) {
		return ChatSerializer.a("{\"text\": \"" + text + "\"}");
	}

}
