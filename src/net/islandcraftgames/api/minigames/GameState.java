package net.islandcraftgames.api.minigames;

public enum GameState {
	
	PRELOAD, WAITING, PLAYING, ENDING;

}
