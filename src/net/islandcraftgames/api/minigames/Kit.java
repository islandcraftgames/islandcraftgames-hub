package net.islandcraftgames.api.minigames;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permission;

import net.islandcraftgames.hub.Main;

public class Kit {

	private GameType type;
	private int id;
	private String langCode;
	protected ItemStack[] items;
	private ItemStack showcaseItem;
	private double price;
	private Kit beforeKit;
	private int slot;
	private Permission perm;

	public Kit(GameType type, int id, String langCode, double price, int beforeKitId, int slot, String permission,
			ItemStack showcaseItem, ItemStack... items) {
		setType(type);
		setID(id);
		setLangCode(langCode);
		setPrice(price);
		if (beforeKitId > -1)
			setBeforeKit(Main.get().getKitManager().getKit(type, beforeKitId));
		setSlot(slot);
		setPermission(permission);
		setShowCaseItem(showcaseItem);
		setItems(items);
	}

	public int getID() {
		return id;
	}

	public String getLangCode() {
		return langCode;
	}

	public ItemStack[] getItems(Player p) {
		return items;
	}

	public ItemStack getShowCaseItem() {
		return showcaseItem;
	}

	public double getPrice() {
		return price;
	}

	public Kit getBeforeKit() {
		return beforeKit;
	}

	public int getSlot() {
		return slot;
	}

	public Permission getPerm() {
		return perm;
	}

	public GameType getGameType() {
		return type;
	}

	protected void setID(int id) {
		this.id = id;
	}

	protected void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	protected void setItems(ItemStack[] items) {
		this.items = items;
	}

	protected void setShowCaseItem(ItemStack showcaseItem) {
		this.showcaseItem = showcaseItem;
	}

	protected void setPrice(double price) {
		this.price = price;
	}

	protected void setBeforeKit(Kit beforeKit) {
		this.beforeKit = beforeKit;
	}

	protected void setSlot(int slot) {
		this.slot = slot;
	}

	protected void setPermission(String perm) {
		this.perm = new Permission(perm);
	}

	protected void setType(GameType type) {
		this.type = type;
	}

}
