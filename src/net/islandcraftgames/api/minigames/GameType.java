package net.islandcraftgames.api.minigames;

public enum GameType {
	SKYWARS(GameStyle.SOLO, "SkyWars", "sw"), CAPTURETHEFLAG(GameStyle.TEAM, "CaptureTheFlag", "ctf"), PAINTWARS(
			GameStyle.TEAM, "PaintWars",
			"pw"), EGGWARS(GameStyle.TEAM, "EggWars", "ew"), X1(GameStyle.SOLO, "1v1", "1v1"), CRAZYRACE(GameStyle.SOLO,
					"CrazyRace", "cr"), ROCKETCRAFT(GameStyle.TEAM, "RocketCraft", "rc");

	private final GameStyle gs;
	private final String fullName;
	private final String alias;

	private GameType(GameStyle gs, String fullName, String alias) {
		this.gs = gs;
		this.fullName = fullName;
		this.alias = alias;
	}

	public GameStyle getGameStyle() {
		return gs;
	}

	public String getFullName() {
		return fullName;
	}

	public String getAlias() {
		return alias;
	}
	
	public static GameType getFromAlias(String alias){
		for(GameType type : values())
			if(type.getAlias().equalsIgnoreCase(alias))
				return type;
		return null;
	}

	public enum GameStyle {
		SOLO, TEAM;
	}
}
