package net.islandcraftgames.api.minigames;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.world.ChunkLoadEvent;

import com.google.common.collect.Lists;

import net.alpenblock.bungeeperms.BungeePerms;
import net.islandcraftgames.api.ItemUtils;
import net.islandcraftgames.api.gui.Gui;
import net.islandcraftgames.api.gui.GuiButton;
import net.islandcraftgames.api.language.LanguageMain;
import net.islandcraftgames.hub.Main;
import net.islandcraftgames.hub.database.MainSource;
import net.islandcraftgames.hub.player.IslandPlayer;
import net.islandcraftgames.hub.player.PlayerData;

public class VillagersManager implements Listener {

	private List<Shop> shops = Lists.newArrayList();

	public VillagersManager() {
		load();
	}

	private void load() {
		Connection conn = MainSource.get().getConnection();
		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM hub_minigames_villagers");
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
				try {
					GameType t = GameType.getFromAlias(rs.getString("gametype"));
					Location l = new Location(Bukkit.getWorld(rs.getString("world")), rs.getDouble("x"),
							rs.getDouble("y"), rs.getDouble("z"), rs.getFloat("yaw"), rs.getFloat("pitch"));
					shops.add(new Shop(l, t));
				} catch (Exception e) {
					e.printStackTrace();
				}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for (Shop shop : shops)
			if (!shop.isSpawned())
				if (shop.getLoc().getChunk().isLoaded())
					spawnVillager(shop);
	}

	public void unload() {
		for (World w : Bukkit.getWorlds())
			for (Entity e : w.getEntities())
				for (Shop shop : shops)
					if (shop.getUuid().equals(e.getUniqueId()))
						e.remove();
		shops.clear();
	}

	private Shop getByUUID(UUID uuid) {
		for (Shop shop : shops)
			if (shop.getUuid().equals(uuid))
				return shop;
		return null;
	}

	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (getByUUID(e.getEntity().getUniqueId()) != null)
			e.setCancelled(true);
	}

	@EventHandler
	public void onInteract(PlayerInteractEntityEvent e) {
		Shop shop = getByUUID(e.getRightClicked().getUniqueId());
		if (shop == null)
			return;
		e.setCancelled(true);
		GameType type = shop.getType();
		if (type == null)
			return;
		Player p = e.getPlayer();
		IslandPlayer ip = PlayerData.getData(p);
		Gui gui = new Gui(Main.get().getKitManager().getShopRows(type), ChatColor.YELLOW + type.getFullName());
		for (Kit kit : Main.get().getKitManager().getAll(type)) {
			gui.setButton(kit.getSlot(),
					new GuiButton(
							ItemUtils
									.setLore(
											ItemUtils.setName(kit.getShowCaseItem(),
													(p.hasPermission(kit.getLangCode()) ? ChatColor.GREEN
															: ChatColor.RED) + LanguageMain.get(p, kit.getLangCode())),
											ChatColor.GOLD + "$" + kit.getPrice())).setListener(event -> {
												if (p.hasPermission(kit.getLangCode())) {
													ip.sendMessage("shop.alreadybought",
															LanguageMain.get(ip, kit.getLangCode()));
												} else if (ip.getMoney() < kit.getPrice())
													ip.sendMessage("shop.nomoney", kit.getPrice());
												else {
													if (kit.getBeforeKit() != null
															&& !p.hasPermission(kit.getBeforeKit().getLangCode()))
														ip.sendMessage("shop.before",
																LanguageMain.get(ip, kit.getLangCode()),
																LanguageMain.get(ip, kit.getBeforeKit().getLangCode()));
													else {
														BungeePerms.getInstance().getPermissionsManager().addUserPerm(
																BungeePerms.getInstance().getPermissionsManager()
																		.getUser(p.getUniqueId()),
																kit.getLangCode());
														ip.addMoney(-kit.getPrice());
														ip.sendMessage("shop.bought",
																LanguageMain.get(ip, kit.getLangCode()),
																kit.getPrice());
													}
												}
												p.closeInventory();
											}));
		}
		gui.open(p);
	}

	@EventHandler
	public void onChunkLoad(ChunkLoadEvent e) {
		for (Shop shop : shops)
			if (shop.getLoc().getChunk().equals(e.getChunk()) && !shop.isSpawned())
				spawnVillager(shop);
	}

	private void spawnVillager(Shop shop) {
		Villager v = ShopVillager.spawn(shop.getLoc());
		shop.setUUID(v.getUniqueId());
		v.setAdult();
		v.setAgeLock(true);
		v.setCustomName(ChatColor.YELLOW + shop.getType().getFullName());
		v.setCustomNameVisible(true);
		v.setRemoveWhenFarAway(false);
		shop.getLoc().getChunk().unload();
		shop.getLoc().getChunk().load();
	}

	private class Shop {
		private final Location loc;
		private UUID uuid;
		private final GameType type;

		public Shop(Location loc, GameType type) {
			this.loc = loc;
			this.type = type;
		}

		public Location getLoc() {
			return loc;
		}

		public UUID getUuid() {
			return uuid;
		}

		public void setUUID(UUID uuid) {
			this.uuid = uuid;
		}

		public GameType getType() {
			return type;
		}

		public boolean isSpawned() {
			return uuid != null;
		}

	}

}
