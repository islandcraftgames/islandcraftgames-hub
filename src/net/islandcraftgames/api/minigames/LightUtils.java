package net.islandcraftgames.api.minigames;

import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.util.Vector;

import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.WorldServer;

public class LightUtils {

	public static void relight(World world, Vector max, Vector min) {
		WorldServer nmsWorld = ((CraftWorld) world).getHandle();
		int topBlockX = (max.getBlockX() < min.getBlockX() ? min.getBlockX() : max.getBlockX());
		int bottomBlockX = (max.getBlockX() > min.getBlockX() ? min.getBlockX() : max.getBlockX());

		int topBlockY = (max.getBlockY() < min.getBlockY() ? min.getBlockY() : max.getBlockY());
		int bottomBlockY = (max.getBlockY() > min.getBlockY() ? min.getBlockY() : max.getBlockY());

		int topBlockZ = (max.getBlockZ() < min.getBlockZ() ? min.getBlockZ() : max.getBlockZ());
		int bottomBlockZ = (max.getBlockZ() > min.getBlockZ() ? min.getBlockZ() : max.getBlockZ());

		for (int x = bottomBlockX; x <= topBlockX; x++)
			for (int z = bottomBlockZ; z <= topBlockZ; z++)
				for (int y = bottomBlockY; y <= topBlockY; y++)
					nmsWorld.x(new BlockPosition(x, y, z));
	}

}
