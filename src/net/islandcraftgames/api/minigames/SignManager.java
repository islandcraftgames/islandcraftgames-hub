package net.islandcraftgames.api.minigames;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import net.islandcraftgames.api.LocationUtils;
import net.islandcraftgames.hub.database.MainSource;

public class SignManager {

	private Multimap<GameType, GameSign> signs = ArrayListMultimap.create();

	public SignManager() {
		Connection conn = MainSource.get().getConnection();
		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM hub_minigames_signs");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				try {
					Location l = new Location(Bukkit.getWorld(rs.getString("world")), rs.getInt("x"), rs.getInt("y"),
							rs.getInt("z"));
					GameType type = GameType.getFromAlias(rs.getString("gametype"));
					signs.put(type, new GameSign(rs.getInt("id"), l, rs.getString("map"), type));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public class GameSign {
		private final Location loc;
		private final String map;
		private final int id;
		private final GameType type;

		public GameSign(int id, Location loc, String map, GameType type) {
			this.loc = loc;
			this.map = map;
			this.id = id;
			this.type = type;
		}

		public String getMap() {
			return map;
		}

		public Location getLocation() {
			return loc;
		}

		public int getId() {
			return id;
		}

		public GameType getType() {
			return type;
		}

		public void update() {
			// TODO Make the sign update for each player.
		}
		
		public void delete(){
			signs.remove(type, this);
			Connection conn = MainSource.get().getConnection();
			try {
				PreparedStatement stmt = conn.prepareStatement("DELETE FROM hub_minigames_signs WHERE id=?");
				stmt.setInt(1, id);
				stmt.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public void addSign(GameType type, Location loc, String map) {
		Connection conn = MainSource.get().getConnection();
		try {
			PreparedStatement stmt = conn.prepareStatement(
					"INSERT INTO hub_minigames_signs (gametype, map, world, x, y, z) VALUES(?, ?, ?, ?, ?, ?)");
			stmt.setString(1, type.getAlias());
			stmt.setString(2, map);
			stmt.setString(3, loc.getWorld().getName());
			stmt.setInt(4, loc.getBlockX());
			stmt.setInt(5, loc.getBlockY());
			stmt.setInt(6, loc.getBlockZ());
			stmt.executeUpdate();
			stmt = conn.prepareStatement("SELECT LAST_INSERT_ID() AS id");
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
				signs.put(type, new GameSign(rs.getInt("id"), loc, map, type));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Collection<GameSign> getAll(GameType type) {
		return signs.get(type);
	}
	
	public GameSign getByLocation(Location loc){
		for (GameSign sign : signs.values())
			if (LocationUtils.isBlockEquals(loc, sign.getLocation()))
				return sign;
		return null;
	}

	public boolean exists(GameType type, Location location) {
		for (GameSign sign : getAll(type))
			if (LocationUtils.isBlockEquals(location, sign.getLocation()))
				return true;
		return false;
	}

}
