package net.islandcraftgames.api.minigames;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

public class KitManager {

	private Multimap<GameType, Kit> kits = ArrayListMultimap.create();
	private HashMap<GameType, Integer> sizes = Maps.newHashMap();

	public void addKit(GameType type, Kit kit) {
		kits.put(type, kit);
	}

	public ArrayList<Kit> getAll(GameType type) {
		return new ArrayList<Kit>(kits.get(type));
	}

	public boolean exists(GameType type, int id) {
		return getKit(type, id) != null;
	}

	public Kit getKit(GameType type, int id) {
		for (Kit kit : kits.get(type))
			if (kit.getID() == id)
				return kit;
		return null;
	}

	public int getShopRows(GameType type) {
		return sizes.containsKey(type) ? sizes.get(type) : 6;
	}

	public void setShopRows(GameType type, int rows) {
		sizes.put(type, rows);
	}

}
