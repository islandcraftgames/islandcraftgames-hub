package net.islandcraftgames.api.minigames;

import java.util.Map;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import net.islandcraftgames.api.ReflectionUtils;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityTypes;

public enum CustomEntityManager {
	SHOPVILLAGER("Villager", 120, ShopVillager.class);

	private CustomEntityManager(String name, int id, Class<? extends Entity> custom) {
		addToMaps(custom, name, id);
	}

	public static void spawnEntity(Entity entity, Location loc) {
		entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
		((CraftWorld) loc.getWorld()).getHandle().addEntity(entity);
	}

	@SuppressWarnings("unchecked")
	private static void addToMaps(Class<? extends Entity> clazz, String name, int id) {
		((Map<String, Class<? extends Entity>>) ReflectionUtils.getPrivateField("c", EntityTypes.class, null)).put(name,
				clazz);
		((Map<Class<? extends Entity>, String>) ReflectionUtils.getPrivateField("d", EntityTypes.class, null))
				.put(clazz, name);
		((Map<Class<? extends Entity>, Integer>) ReflectionUtils.getPrivateField("f", EntityTypes.class, null))
				.put(clazz, Integer.valueOf(id));
	}
}
