package net.islandcraftgames.api.minigames;

import java.io.File;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;

import com.boydti.fawe.bukkit.wrapper.AsyncWorld;
import com.google.common.collect.Lists;

import net.islandcraftgames.hub.Main;

public class WorldManager {

	private static WorldManager instance;

	private boolean switchW = true;
	private List<Location> posLocs = Lists.newArrayList();

	public Location getEmptyLocation() {
		checkWorld();
		Location l = posLocs.get(0);
		posLocs.remove(0);
		return l;
	}

	public void checkWorld() {
		if (posLocs.size() > 5)
			return;
		Bukkit.getScheduler().runTaskAsynchronously(Main.get(), new Runnable() {
			@Override
			public void run() {
				createWorld();
			}
		});
	}

	private World createWorld() {
		switchW = !switchW;
		String worldName = "minigames-" + (switchW ? 1 : 0);
		if (Bukkit.getWorld(worldName) != null)
			Bukkit.unloadWorld(worldName, false);
		File file = new File(".", worldName);
		if (file.exists())
			file.delete();
		AsyncWorld world = AsyncWorld
				.create(new WorldCreator(worldName).generator("IslandCraftGames").environment(Environment.NORMAL));
		world.setMonsterSpawnLimit(0);
		world.setAmbientSpawnLimit(0);
		world.setAnimalSpawnLimit(0);
		world.setAutoSave(false);
		world.setGameRuleValue("doMobSpawning", "false");
		world.setDifficulty(Difficulty.EASY);
		world.setGameRuleValue("doDaylightCycle", "false");
		world.setTime(6000);
		world.setWeatherDuration(Integer.MAX_VALUE);
		world.commit();
		for (int x = 1000; x < 50000; x += 1000)
			for (int z = 1000; z < 50000; z += 1000) {
				posLocs.add(new Location(world.getParent(), x, 64, z));
				posLocs.add(new Location(world.getParent(), -x, 64, -z));
			}
		Main.get().getSchematicManager().getAll(null).get(0).paste(new Location(world.getParent(), 0, 64, 0));
		return world.getParent();
	}

	public static WorldManager get() {
		if (instance == null)
			instance = new WorldManager();
		return instance;
	}

}
