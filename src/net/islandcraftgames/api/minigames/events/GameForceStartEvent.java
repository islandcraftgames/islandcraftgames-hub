package net.islandcraftgames.api.minigames.events;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.api.minigames.GameType;
import net.islandcraftgames.hub.player.IslandPlayer;

public class GameForceStartEvent extends Event implements Cancellable {

	private static final HandlerList handlers = new HandlerList();

	private final Game game;
	private final IslandPlayer gp;

	private boolean cancelled = false;
	private String error = "";

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public GameForceStartEvent(Game game, IslandPlayer gp) {
		this.game = game;
		this.gp = gp;
	}

	public GameType getGameType() {
		return game.getGameType();
	}

	public IslandPlayer getGamePlayer() {
		return gp;
	}

	public Game getGame() {
		return game;
	}

	public void setErrorMessage(String error) {
		this.error = error;
	}

	public String getErrorMessage() {
		return error;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

}
