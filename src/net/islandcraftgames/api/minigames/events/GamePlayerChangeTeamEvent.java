package net.islandcraftgames.api.minigames.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.api.minigames.GameTeam;
import net.islandcraftgames.api.minigames.GameType;
import net.islandcraftgames.hub.player.IslandPlayer;

public class GamePlayerChangeTeamEvent extends Event {

	private static final HandlerList handlers = new HandlerList();

	private final Game game;
	private final IslandPlayer gp;
	private final GameTeam oldTeam;
	private final GameTeam newTeam;

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public GamePlayerChangeTeamEvent(Game game, IslandPlayer gp, GameTeam oldTeam, GameTeam newTeam) {
		this.game = game;
		this.gp = gp;
		this.newTeam = newTeam;
		this.oldTeam = oldTeam;
	}

	public GameType getGameType() {
		return game.getGameType();
	}

	public IslandPlayer getGamePlayer() {
		return gp;
	}

	public Game getGame() {
		return game;
	}

	public GameTeam getOldTeam() {
		return oldTeam;
	}

	public GameTeam getNewTeam() {
		return newTeam;
	}

}
