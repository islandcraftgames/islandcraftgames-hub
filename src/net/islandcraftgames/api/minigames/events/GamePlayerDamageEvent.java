package net.islandcraftgames.api.minigames.events;

import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.api.minigames.GameType;
import net.islandcraftgames.hub.player.IslandPlayer;

public class GamePlayerDamageEvent extends Event implements Cancellable {

	private static final HandlerList handlers = new HandlerList();

	private final Game game;
	private final IslandPlayer gp;
	private final DamageCause cause;
	private final Entity damager;
	private double damage;
	private boolean cancelled = false;

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public GamePlayerDamageEvent(Game game, IslandPlayer gp, DamageCause cause, double damage, Entity damager) {
		this.game = game;
		this.gp = gp;
		this.cause = cause;
		this.damager = damager;
		this.damage = damage;
	}

	public GameType getGameType() {
		return game.getGameType();
	}

	public IslandPlayer getGamePlayer() {
		return gp;
	}

	public Game getGame() {
		return game;
	}

	public double getDamage() {
		return damage;
	}

	public void setDamage(double damage) {
		this.damage = damage;
	}

	public DamageCause getCause() {
		return cause;
	}

	public Entity getDamager() {
		return damager;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

}
