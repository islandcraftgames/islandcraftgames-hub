package net.islandcraftgames.api.minigames.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.api.minigames.GameType;

public class GameEndEvent extends Event{
	
	private static final HandlerList handlers = new HandlerList();
	
	private Game game;

	public HandlerList getHandlers() {
	    return handlers;
	}

	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	public GameEndEvent(Game game){
		this.game = game;
	}
	
	public GameType getGameType(){
		return game.getGameType();
	}
	
	public Game getGame(){
		return game;
	}

}
