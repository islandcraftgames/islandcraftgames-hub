package net.islandcraftgames.api.minigames.events;

import org.bukkit.Location;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.api.minigames.GameType;
import net.islandcraftgames.hub.player.IslandPlayer;

public class GamePlayerDeathEvent extends Event{
	
	private static final HandlerList handlers = new HandlerList();
	
	private final Game game;
	private final IslandPlayer gp;
	private Location respawnLocation;
	private final IslandPlayer killer;

	public HandlerList getHandlers() {
	    return handlers;
	}

	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	public GamePlayerDeathEvent(Game game, IslandPlayer gp){
		this.game = game;
		this.gp = gp;
		this.respawnLocation = game.getCenter();
		this.killer = game.getLastHit(gp);
	}
	
	public GameType getGameType(){
		return game.getGameType();
	}
	
	public IslandPlayer getGamePlayer(){
		return gp;
	}
	
	public Game getGame(){
		return game;
	}

	public Location getRespawnLocation() {
		return respawnLocation;
	}

	public void setRespawnLocation(Location respawnLocation) {
		this.respawnLocation = respawnLocation;
	}

	public IslandPlayer getKiller() {
		return killer;
	}

}
