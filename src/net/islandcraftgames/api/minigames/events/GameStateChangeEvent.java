package net.islandcraftgames.api.minigames.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.api.minigames.GameState;
import net.islandcraftgames.api.minigames.GameType;

public class GameStateChangeEvent extends Event {

	private static final HandlerList handlers = new HandlerList();

	private final Game game;
	private final GameState oldState;
	private final GameState newState;

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public GameStateChangeEvent(Game game, GameState oldState, GameState newState) {
		this.game = game;
		this.oldState = oldState;
		this.newState = newState;
	}

	public GameType getGameType() {
		return game.getGameType();
	}

	public Game getGame() {
		return game;
	}

	public GameState getOldState() {
		return oldState;
	}

	public GameState getNewState() {
		return newState;
	}

}
