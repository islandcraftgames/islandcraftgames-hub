package net.islandcraftgames.api.minigames.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.api.minigames.GameType;
import net.islandcraftgames.hub.player.IslandPlayer;

public class GamePlayerLeaveEvent extends Event{
	
	private static final HandlerList handlers = new HandlerList();
	
	private final Game game;
	private final IslandPlayer gp;

	public HandlerList getHandlers() {
	    return handlers;
	}

	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	public GamePlayerLeaveEvent(Game game, IslandPlayer gp){
		this.game = game;
		this.gp = gp;
	}
	
	public GameType getGameType(){
		return game.getGameType();
	}
	
	public IslandPlayer getGamePlayer(){
		return gp;
	}
	
	public Game getGame(){
		return game;
	}

}
