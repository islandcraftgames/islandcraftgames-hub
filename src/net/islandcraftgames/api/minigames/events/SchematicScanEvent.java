package net.islandcraftgames.api.minigames.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.Vector;

import net.islandcraftgames.api.minigames.GameType;
import net.islandcraftgames.api.minigames.SchematicManager.GameMap;

@SuppressWarnings("deprecation")
public class SchematicScanEvent extends Event {

	private static final HandlerList handlers = new HandlerList();

	private final GameType type;
	private final Vector block;
	private final GameMap gameMap;
	private final CuboidClipboard cc;

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	
	public SchematicScanEvent(GameType type, GameMap gameMap, CuboidClipboard cc, Vector block) {
		this.type = type;
		this.block = block;
		this.gameMap = gameMap;
		this.cc = cc;
	}

	public GameType getGameType() {
		return type;
	}

	public Vector getBlock() {
		return block;
	}

	public GameMap getGameMap() {
		return gameMap;
	}
	
	public CuboidClipboard getClipboard(){
		return cc;
	}

}
