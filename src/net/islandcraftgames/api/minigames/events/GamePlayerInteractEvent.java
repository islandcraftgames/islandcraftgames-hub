package net.islandcraftgames.api.minigames.events;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.api.minigames.GameType;
import net.islandcraftgames.hub.player.IslandPlayer;

public class GamePlayerInteractEvent extends PlayerInteractEvent {

	private static final HandlerList handlers = new HandlerList();

	private final Game game;
	private final IslandPlayer gp;

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public GamePlayerInteractEvent(Game game, IslandPlayer gp, Action action, ItemStack item, Block block,
			BlockFace face) {
		super(gp.toBukkit(), action, item, block, face);
		this.game = game;
		this.gp = gp;
	}

	public GameType getGameType() {
		return game.getGameType();
	}

	public IslandPlayer getGamePlayer() {
		return gp;
	}

	public Game getGame() {
		return game;
	}

}
