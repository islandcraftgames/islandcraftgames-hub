package net.islandcraftgames.api.minigames.events;

import org.bukkit.Location;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerMoveEvent;

import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.api.minigames.GameType;
import net.islandcraftgames.hub.player.IslandPlayer;

public class GamePlayerMoveEvent extends PlayerMoveEvent{
	
	private static final HandlerList handlers = new HandlerList();
	
	private final Game game;
	private final IslandPlayer gp;

	public HandlerList getHandlers() {
	    return handlers;
	}

	public static HandlerList getHandlerList() {
	    return handlers;
	}
	
	public GamePlayerMoveEvent(Game game, IslandPlayer gp, Location to, Location from){
		super(gp.toBukkit(), to, from);
		this.game = game;
		this.gp = gp;
	}
	
	public GameType getGameType(){
		return game.getGameType();
	}
	
	public IslandPlayer getGamePlayer(){
		return gp;
	}
	
	public Game getGame(){
		return game;
	}

}
