package net.islandcraftgames.api.minigames;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import net.islandcraftgames.hub.Main;

public class GameManager {

	private Multimap<GameType, Game> signs = ArrayListMultimap.create();
	
	public Game getGame(GameType type, String mapName){
		boolean exists = Main.get().getSchematicManager().exists(type, mapName);
		for(Game game : getAll(type))
			if(game.getMapName().equalsIgnoreCase(mapName) || !exists)
				return game;
		return createGame(type, mapName);
	}
	
	private Game createGame(GameType type, String mapName) {
		Game game = new Game(type, Main.get().getSchematicManager().getMapWithRandom(type, mapName));
		signs.put(type, game);
		return game;
	}

	public void deleteGame(Game game){
		signs.remove(game.getGameType(), game);
	}

	public List<Game> getAll(GameType type) {
		return new ArrayList<Game>(signs.get(type));
	}

	public List<Game> getAll() {
		return new ArrayList<Game>(signs.values());
	}
	
}
