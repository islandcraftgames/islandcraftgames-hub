package net.islandcraftgames.api.minigames;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.boydti.fawe.FaweAPI;
import com.boydti.fawe.util.EditSessionBuilder;
import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.extent.Extent;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.transform.BlockTransformExtent;
import com.sk89q.worldedit.function.mask.ExistingBlockMask;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.transform.Transform;
import com.sk89q.worldedit.schematic.SchematicFormat;
import com.sk89q.worldedit.world.World;

import net.islandcraftgames.api.minigames.events.SchematicScanEvent;
import net.islandcraftgames.hub.Main;

@SuppressWarnings("deprecation")
public class SchematicManager {

	private Multimap<GameType, GameMap> schematics = ArrayListMultimap.create();

	public void searchFolder(File schematicFolder, GameType type) {
		for (File schem : schematicFolder.listFiles()) {
			try {
				if (!schem.getName().endsWith(".schematic"))
					continue;

				Clipboard cc = ClipboardFormat.SCHEMATIC.getReader(new FileInputStream(schem)).read(null);
				GameMap gm;
				schematics.put(type, gm = new GameMap(schem.getName().replace(".schematic", ""), cc, cc.getOrigin()));
				CuboidClipboard ccc = SchematicFormat.getFormat(schem).load(schem);
				Bukkit.getScheduler().runTaskLaterAsynchronously(Main.get(), new Runnable() {
					@Override
					public void run() {
						for (int x = 0; x < ccc.getSize().getBlockX(); x++)
							for (int y = 0; y < ccc.getSize().getBlockY(); y++)
								for (int z = 0; z < ccc.getSize().getBlockZ(); z++)
									Bukkit.getPluginManager()
											.callEvent(new SchematicScanEvent(type, gm, ccc, new Vector(x, y, z)));
					}
				}, 100L);
			} catch (Exception e) {
				Main.get().getLogger()
						.warning(String.format("Failed to load schematic %s: %s", schem.getName(), e.getMessage()));
			}
		}
	}

	public class GameMap {
		private final String name;
		private final Clipboard map;
		private final Vector offset;
		private final Map<String, Object> data = Maps.newHashMap();

		private GameMap(String name, Clipboard map, Vector offset) {
			this.name = name;
			this.map = map;
			this.offset = offset;
		}

		public Clipboard getMap() {
			return map;
		}

		public String getName() {
			return name;
		}

		public EditSession paste(Location loc) {
			return paste(FaweAPI.getWorld(loc.getWorld().getName()), new Vector(loc.getX(), loc.getY(), loc.getZ()),
					false, true, null);
		}

		public EditSession paste(Location loc, boolean pasteAir) {
			return paste(FaweAPI.getWorld(loc.getWorld().getName()), new Vector(loc.getX(), loc.getY(), loc.getZ()),
					false, pasteAir, null);
		}

		public EditSession paste(Location loc, boolean pasteAir, @Nullable Transform transform) {
			return paste(FaweAPI.getWorld(loc.getWorld().getName()), new Vector(loc.getX(), loc.getY(), loc.getZ()),
					false, pasteAir, transform);
		}

		public EditSession paste(World world, Vector to, boolean allowUndo, boolean pasteAir,
				@Nullable Transform transform) {
			Preconditions.checkNotNull(world);
			Preconditions.checkNotNull(to);
			EditSessionBuilder builder = new EditSessionBuilder(world).autoQueue(Boolean.valueOf(true))
					.checkMemory(Boolean.valueOf(false)).allowedRegionsEverywhere().limitUnlimited();
			EditSession editSession;
			if (allowUndo) {
				editSession = builder.build();
			} else {
				editSession = builder.changeSetNull().fastmode(Boolean.valueOf(true)).build();
			}
			Extent extent = this.map;
			if (transform != null) {
				extent = new BlockTransformExtent(this.map, transform, world.getWorldData().getBlockRegistry());
			}
			ForwardExtentCopy copy = new ForwardExtentCopy(extent, this.map.getRegion(), this.map.getOrigin(),
					editSession, to);
			if (transform != null) {
				copy.setTransform(transform);
			}
			if (!pasteAir) {
				copy.setSourceMask(new ExistingBlockMask(this.map));
			}
			try {
				Operations.completeLegacy(copy);
			} catch (MaxChangedBlocksException e) {
				e.printStackTrace();
			}
			editSession.flushQueue();
			return editSession;
		}

		public Vector getOffset() {
			return offset;
		}

		public Map<String, Object> getData() {
			return data;
		}

		public Object getData(String key) {
			return data.get(key);
		}

		@SuppressWarnings("unchecked")
		public List<Vector> getVectorListData(String key) {
			Object spawnsObj = getData(key);
			if (spawnsObj == null || !(spawnsObj instanceof List<?>))
				spawnsObj = Lists.newArrayList();
			return (List<Vector>) spawnsObj;
		}

		public void setData(String key, Object value) {
			data.put(key, value);
		}

	}

	public ArrayList<GameMap> getAll(GameType type) {
		return new ArrayList<GameMap>(schematics.get(type));
	}

	public boolean exists(GameType type, String name) {
		return getMap(type, name) != null;
	}

	public GameMap getMap(GameType type, String name) {
		for (GameMap map : schematics.get(type))
			if (map.getName().equals(name))
				return map;
		return null;
	}

	public GameMap getMapWithRandom(GameType type, String name) {
		return exists(type, name) ? getMap(type, name) : getAll(type).get(new Random().nextInt(getAll(type).size()));
	}

}
