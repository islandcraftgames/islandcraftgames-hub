package net.islandcraftgames.api.minigames;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Villager;

import net.islandcraftgames.api.ReflectionUtils;
import net.minecraft.server.v1_8_R3.EntityVillager;
import net.minecraft.server.v1_8_R3.PathfinderGoalSelector;
import net.minecraft.server.v1_8_R3.World;

public class ShopVillager extends EntityVillager {

	public ShopVillager(org.bukkit.World w) {
		this(((CraftWorld) w).getHandle());
	}

	public ShopVillager(World world) {
		super(world);

		((List<?>) ReflectionUtils.getPrivateField("b", PathfinderGoalSelector.class, goalSelector)).clear();
		((List<?>) ReflectionUtils.getPrivateField("c", PathfinderGoalSelector.class, goalSelector)).clear();
		((List<?>) ReflectionUtils.getPrivateField("b", PathfinderGoalSelector.class, targetSelector)).clear();
		((List<?>) ReflectionUtils.getPrivateField("c", PathfinderGoalSelector.class, targetSelector)).clear();

		j(false);
	}

	@Override
	public void makeSound(String s, float f, float f1) {
	}

	@Override
	public void g(float f, float f1) {
		if (f != 0 || f1 != 0)
			System.out.println(f + ":" + f1);
	}
	
	public static Villager spawn(Location l) {
		ShopVillager v = new ShopVillager(l.getWorld());
		CustomEntityManager.spawnEntity(v, l);
		return (Villager) v.getBukkitEntity();
	}

}
