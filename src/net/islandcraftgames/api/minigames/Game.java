package net.islandcraftgames.api.minigames;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.islandcraftgames.api.PacketUtils;
import net.islandcraftgames.api.language.LanguageMain;
import net.islandcraftgames.api.minigames.SchematicManager.GameMap;
import net.islandcraftgames.api.minigames.events.GameCreateEvent;
import net.islandcraftgames.api.minigames.events.GameEndEvent;
import net.islandcraftgames.api.minigames.events.GamePlayerJoinEvent;
import net.islandcraftgames.api.minigames.events.GamePlayerLeaveEvent;
import net.islandcraftgames.api.minigames.events.GameStartEvent;
import net.islandcraftgames.api.minigames.events.GameStartLoadingSchematicEvent;
import net.islandcraftgames.api.minigames.events.GameStateChangeEvent;
import net.islandcraftgames.api.minigames.events.SpectatorLeaveEvent;
import net.islandcraftgames.hub.Main;
import net.islandcraftgames.hub.player.IslandPlayer;

public class Game {

	private final GameType type;
	private final GameMap map;
	private final World world;
	private final Location baseLoc;
	private GameState gameState = GameState.PRELOAD;
	private List<IslandPlayer> players = Lists.newArrayList();
	private int timer = 0;
	private int maximumPlayers = 1;
	private final Map<String, Object> data = Maps.newHashMap();
	private Vector minVec;
	private Vector maxVec;
	private List<IslandPlayer> spectators = Lists.newArrayList();
	private Map<IslandPlayer, IslandPlayer> lastHits = Maps.newHashMap();

	public Game(GameType type, GameMap map) {
		this.type = type;
		this.map = map;
		this.baseLoc = WorldManager.get().getEmptyLocation();
		this.world = baseLoc.getWorld();
		Bukkit.getPluginManager().callEvent(new GameCreateEvent(this));
		Bukkit.getPluginManager().callEvent(new GameStartLoadingSchematicEvent(Game.this));
		Bukkit.getScheduler().runTaskAsynchronously(Main.get(), new Runnable() {
			@Override
			public void run() {
				map.paste(baseLoc);
				setGameState(GameState.WAITING);
			}
		});
	}

	public Vector getMinVector() {
		return this.minVec;
	}

	public Vector getMaxVector() {
		return this.maxVec;
	}

	public GameMap getGameMap() {
		return map;
	}

	public GameType getGameType() {
		return this.type;
	}

	public World getWorld() {
		return world;
	}

	public GameState getGameState() {
		return gameState;
	}

	public String getMapName() {
		return map.getName();
	}

	public void setGameState(GameState gameState) {
		Bukkit.getPluginManager().callEvent(new GameStateChangeEvent(this, this.gameState, gameState));
		this.gameState = gameState;
	}

	public void startGame() {
		setGameState(GameState.PLAYING);
		sendMessage("game.start");
		Bukkit.getPluginManager().callEvent(new GameStartEvent(this));
	}

	public void addPlayer(IslandPlayer ip) {
		ip.setXpOnBukkit();
		Player p = ip.toBukkit();
		p.setGameMode(GameMode.SURVIVAL);
		p.setFlying(false);
		p.setAllowFlight(false);
		p.setMaxHealth(20);
		p.setHealth(20);
		p.setFoodLevel(20);
		players.add(ip);
		ip.setGame(this);
		p.teleport(new Location(this.world, 0.5, 65, 0.5));
		Bukkit.getPluginManager().callEvent(new GamePlayerJoinEvent(this, ip));
		sendMessage("game.join", ip.toBukkit().getDisplayName(), this.players.size(), getMaximumPlayers());
	}

	public void removePlayer(IslandPlayer ip) {
		removePlayer(ip, true);
	}

	public void removePlayer(IslandPlayer ip, boolean fireEvent) {
		ip.setXpOnBukkit();
		Player p = ip.toBukkit();
		p.setGameMode(GameMode.ADVENTURE);
		p.setFlying(false);
		p.setAllowFlight(false);
		p.setMaxHealth(20);
		p.setHealth(20);
		p.setFoodLevel(20);
		players.remove(ip);
		ip.setGame(null);
		if (fireEvent)
			Bukkit.getPluginManager().callEvent(new GamePlayerLeaveEvent(this, ip));
		if (getGameState() == GameState.WAITING)
			sendMessage("game.leave", ip.toBukkit().getDisplayName(), this.players.size(), getMaximumPlayers());
	}

	public void addSpectator(IslandPlayer ip) {
		spectators.add(ip);
		ip.setGame(this);
		ip.toBukkit().setGameMode(GameMode.SPECTATOR);
		ip.toBukkit().teleport(baseLoc);
	}

	public boolean isSpectating(IslandPlayer ip) {
		return spectators.contains(ip);
	}

	public void removeSpectator(IslandPlayer ip) {
		spectators.remove(ip);
		ip.setGame(null);
		Player p = ip.toBukkit();
		p.setGameMode(GameMode.ADVENTURE);
		p.setFlying(false);
		p.setAllowFlight(false);
		p.setMaxHealth(20);
		p.setHealth(20);
		p.setFoodLevel(20);
		Bukkit.getPluginManager().callEvent(new SpectatorLeaveEvent(this, ip));
	}

	public List<IslandPlayer> getSpectators() {
		return new ArrayList<IslandPlayer>(spectators);
	}

	public void sendMessage(String messageCode, Object... args) {
		for (IslandPlayer ip : players)
			ip.toBukkit().sendMessage(LanguageMain.get(ip, messageCode, args));
	}

	public void sendTitle(String title, String subtitle, int fadeIn, int stay, int fadeOut) {
		for (IslandPlayer ip : players)
			PacketUtils.sendTitle(ip.toBukkit(), title, subtitle, fadeIn, stay, fadeOut);
	}

	public void setTimer(int timer) {
		this.timer = timer;
	}

	public void decreaseTimer() {
		this.timer--;
	}

	public int getTimer() {
		return this.timer;
	}

	public int getMaximumPlayers() {
		return maximumPlayers;
	}

	public void setMaximumPlayers(int maximumPlayers) {
		this.maximumPlayers = maximumPlayers;
	}

	public List<IslandPlayer> getPlayers() {
		return new ArrayList<IslandPlayer>(players);
	}

	public Location getCenter() {
		return this.baseLoc.clone();
	}

	public int getPlayingAmount() {
		return players.size();
	}

	public void setLastHit(IslandPlayer victim, IslandPlayer hitter) {
		lastHits.put(victim, hitter);
	}

	public IslandPlayer getLastHit(IslandPlayer victim) {
		return lastHits.get(victim);
	}

	public void endGame() {
		setGameState(GameState.ENDING);
		Bukkit.getPluginManager().callEvent(new GameEndEvent(this));
	}

	/* Data Management */

	public Map<String, Object> getData() {
		return data;
	}

	public Object getData(String key) {
		return data.get(key);
	}

	@SuppressWarnings("unchecked")
	public List<Location> getLocationListData(String key) {
		Object spawnsObj = getData(key);
		if (spawnsObj == null || !(spawnsObj instanceof List<?>))
			spawnsObj = Lists.newArrayList();
		return (List<Location>) spawnsObj;
	}

	public void setData(String key, Object value) {
		data.put(key, value);
	}

}
