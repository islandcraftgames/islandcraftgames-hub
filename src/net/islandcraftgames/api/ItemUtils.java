package net.islandcraftgames.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemUtils {

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull String displayName) {
		ItemStack a = new ItemStack(material, amount);
		if (displayName.length() > 0) {
			ItemMeta b = a.getItemMeta();
			b.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
			a.setItemMeta(b);
		}
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull String displayName,
			@Nonnull String... lore) {
		ItemStack a = new ItemStack(material, amount);
		ItemMeta b = a.getItemMeta();
		if (displayName.length() > 0)
			b.setDisplayName(displayName);
		List<String> c = new ArrayList<String>();
		for (String d : lore)
			c.add(d);
		b.setLore(c);
		a.setItemMeta(b);
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull ItemMeta previousMeta,
			@Nonnull String displayName, @Nonnull String... lore) {
		ItemStack a = new ItemStack(material, amount);
		ItemMeta b = previousMeta;
		if (displayName.length() > 0)
			b.setDisplayName(displayName);
		List<String> c = new ArrayList<String>();
		for (String d : lore)
			c.add(d);
		b.setLore(c);
		a.setItemMeta(b);
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull int durability,
			@Nonnull String displayName) {
		ItemStack a = createItem(material, amount, displayName);
		a.setDurability((short) durability);
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull int durability,
			@Nonnull String displayName, @Nonnull String... lore) {
		ItemStack a = createItem(material, amount, displayName, lore);
		a.setDurability((short) durability);
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull String displayName,
			@Nonnull Object[]... enchantments) {
		ItemStack a = createItem(material, amount, displayName);
		for (Object[] enc : enchantments) {
			a.addUnsafeEnchantment((Enchantment) enc[0], Integer.parseInt((String) enc[1]));
		}
		return a;
	}

	public static ItemStack createItem(@Nonnull Material material, @Nonnull int amount, @Nonnull short durability,
			@Nonnull String displayName, @Nonnull String[] lore, @Nonnull Object[]... enchantments) {
		ItemStack a = createItem(material, amount, durability, displayName, lore);
		for (Object[] enc : enchantments) {
			if (enc[0] != null)
				a.addUnsafeEnchantment((Enchantment) enc[0], (Integer) enc[1]);
		}
		return a;
	}

	public static ItemStack changeAmount(@Nonnull ItemStack itemStack, @Nonnull int amount) {
		ItemStack a = itemStack.clone();
		a.setAmount(amount);
		return a;
	}

	public static ItemStack setName(@Nonnull ItemStack itemStack, @Nonnull String name) {
		ItemMeta im = itemStack.getItemMeta();
		im.setDisplayName(name);
		itemStack.setItemMeta(im);
		return itemStack;
	}

	public static ItemStack setLore(@Nonnull ItemStack itemStack, @Nonnull String... lore) {
		ItemMeta im = itemStack.getItemMeta();
		im.setLore(Arrays.asList(lore));
		itemStack.setItemMeta(im);
		return itemStack;
	}

}
