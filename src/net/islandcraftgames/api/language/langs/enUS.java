package net.islandcraftgames.api.language.langs;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import net.islandcraftgames.api.language.Language;

public class enUS extends Language {

	public enUS() {
		setName("en_US");
		ItemStack a = new ItemStack(Material.BANNER);
		BannerMeta banner = (BannerMeta) a.getItemMeta();
		banner.setBaseColor(DyeColor.RED);
		banner.addPattern(new Pattern(DyeColor.WHITE, PatternType.STRIPE_SMALL));
		banner.addPattern(new Pattern(DyeColor.BLUE,
				PatternType.SQUARE_TOP_LEFT));
		banner.setDisplayName(ChatColor.GREEN + "English (USA)");
		a.setItemMeta(banner);
		setMeta(banner);
		setStack(a);
		loadMessages();
	}

}
