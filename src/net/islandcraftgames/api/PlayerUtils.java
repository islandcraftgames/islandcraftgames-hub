package net.islandcraftgames.api;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class PlayerUtils {

	public static void clearInventory(Player p, boolean xp) {
		p.getInventory().setArmorContents(new ItemStack[4]);
		p.getInventory().setContents(new ItemStack[36]);
		if (xp)
			p.setTotalExperience(0);
	}

}
