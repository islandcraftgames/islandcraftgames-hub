package net.islandcraftgames.api;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class LocationUtils {

	public static boolean isBlockEquals(Location loc1, Location loc2) {
		return loc1.getBlockX() == loc2.getBlockX() && loc1.getBlockY() == loc2.getBlockY()
				&& loc1.getBlockZ() == loc2.getBlockZ() && loc1.getWorld().equals(loc2.getWorld());
	}

	public static Location fromString(String code) {
		String[] a = code.split(";");
		World w = Bukkit.getWorld(a[0]);
		double x = Double.parseDouble(a[1]);
		double y = Double.parseDouble(a[2]);
		double z = Double.parseDouble(a[3]);
		float yaw = Float.parseFloat(a[4]);
		float pitch = Float.parseFloat(a[5]);
		return new Location(w, x, y, z, yaw, pitch);
	}

}
