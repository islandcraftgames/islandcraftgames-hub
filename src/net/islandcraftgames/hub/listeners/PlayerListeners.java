package net.islandcraftgames.hub.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import net.islandcraftgames.hub.hotbar.HotbarMain;
import net.islandcraftgames.hub.player.PlayerData;

public class PlayerListeners implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		PlayerData.registerPlayer(p);
		HotbarMain.setHotbarItems(p);
		p.setHealth(20);
		p.setFoodLevel(20);
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		PlayerData.unregisterPlayer(p);
	}

	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent e) {
		if (PlayerData.getData((Player) e.getEntity()).getGame() != null)
			e.setCancelled(true);
	}

}
