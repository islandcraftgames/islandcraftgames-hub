package net.islandcraftgames.hub.tasks;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.api.minigames.events.GameSecondEvent;
import net.islandcraftgames.hub.Main;

public class TimerTask extends BukkitRunnable {

	@Override
	public void run() {
		for (Game game : Main.get().getGameManager().getAll())
			Bukkit.getPluginManager().callEvent(new GameSecondEvent(game));
	}

}
