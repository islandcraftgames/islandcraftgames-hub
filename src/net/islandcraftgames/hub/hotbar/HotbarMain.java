package net.islandcraftgames.hub.hotbar;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import net.islandcraftgames.api.ItemUtils;
import net.islandcraftgames.api.PlayerUtils;
import net.islandcraftgames.api.gui.Gui;
import net.islandcraftgames.api.gui.GuiButton;
import net.islandcraftgames.api.language.Language;
import net.islandcraftgames.api.language.LanguageMain;
import net.islandcraftgames.hub.player.PlayerData;

public class HotbarMain implements Listener {

	public static void setHotbarItems(Player p) {
		PlayerUtils.clearInventory(p, true);
		PlayerInventory inv = p.getInventory();
		inv.setItem(8, ItemUtils.createItem(Material.BANNER, 1, LanguageMain.getFlag(p),
				ChatColor.GREEN + LanguageMain.get(p, "main.item.changelang"), ""));
	}

	public static void openLangInv(Player p) {
		Gui gui = new Gui(getSlots(LanguageMain.getAllLangs().size()) / 9, LanguageMain.get(p, "main.inv.selectlang"));
		for (Language bm : LanguageMain.getAllLangs()) {
			gui.addButton(new GuiButton(bm.getStack()).setListener(e -> {
				PlayerData.getData(p).setLang(bm.getName());
				p.sendMessage(ChatColor.GREEN + "Language changed to: "
						+ ChatColor.stripColor(bm.getStack().getItemMeta().getDisplayName()));
				p.closeInventory();
				setHotbarItems(p);
			}));
		}
		gui.open(p);
	}

	private static int getSlots(int i) {
		int actual = 9;
		while (i > 9) {
			i = i - 9;
			actual = actual + 9;
		}
		return actual;
	}

	@EventHandler
	public void onItemClick(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			e.setCancelled(checkInteract(e.getPlayer(), e.getItem()));
		}
	}
	
	@EventHandler
	public void onItemClickEntity(PlayerInteractEntityEvent e){
		e.setCancelled(checkInteract(e.getPlayer(), e.getPlayer().getItemInHand()));
	}

	private boolean checkInteract(Player p, ItemStack is) {
		if (is == null)
			return false;
		if (!is.hasItemMeta())
			return false;
		String dn = ChatColor.stripColor(is.getItemMeta().getDisplayName());
		if (LanguageMain.isLangMessage(dn, "main.item.compass")) {
			// p.openInventory(ItemsMain.getTravelInv(p));
			return true;
		}
		if (LanguageMain.isLangMessage(dn, "main.item.changelang")) {
			openLangInv(p);
			return true;
		}
		if (LanguageMain.isLangMessage(dn, "main.item.vote")) {
			// p.performCommand("vote");
			return true;
		}
		return false;
	}

}
