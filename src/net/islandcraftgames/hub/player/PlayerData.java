package net.islandcraftgames.hub.player;

import java.util.Map;

import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

public class PlayerData {

	private static PlayerData instance;

	private static PlayerData get() {
		if (instance == null)
			instance = new PlayerData();
		return instance;
	}

	private Map<Player, IslandPlayer> players = Maps.newHashMap();

	public static IslandPlayer getData(Player p) {
		return get().players.get(p);
	}
	
	public static void registerPlayer(Player p){
		if(get().players.containsKey(p))return;
		IslandPlayer ip = new IslandPlayer(p);
		ip.load();
		ip.loadMinigamesScore();
		setPlayer(p, ip);
	}

	public static void setPlayer(Player p, IslandPlayer ip) {
		get().players.put(p, ip);
	}
	
	public static void unregisterPlayer(Player p){
		if(!get().players.containsKey(p))return;
		IslandPlayer ip = getData(p);
		ip.save();
		removePlayer(p);
	}

	public static IslandPlayer removePlayer(Player p) {
		return get().players.remove(p);
	}

}
