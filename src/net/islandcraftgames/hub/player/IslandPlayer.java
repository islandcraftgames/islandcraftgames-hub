package net.islandcraftgames.hub.player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

import net.islandcraftgames.api.language.LanguageMain;
import net.islandcraftgames.api.minigames.Game;
import net.islandcraftgames.hub.database.MainSource;

public class IslandPlayer {

	private final Player bukkit;

	private String lang = "en_UK";
	private boolean firstLogin = true;
	private int xp = 0;
	private double money = 0.0;
	private int dbId = 0;
	private Game game;
	private Map<String, MinigameData> minigameData = Maps.newHashMap();

	public IslandPlayer(Player p) {
		this.bukkit = p;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public boolean isPlaying() {
		return game != null && !game.isSpectating(this);
	}

	public boolean isSpectating() {
		return game != null && game.isSpectating(this);
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public boolean isFirstLogin() {
		return firstLogin;
	}

	public int getXp() {
		return xp;
	}

	public void setXp(int xp) {
		this.xp = xp;
	}

	public void addXp(int xp) {
		this.xp += xp;
	}

	public void setXpOnBukkit() {
		bukkit.setTotalExperience(0);
		bukkit.setLevel(0);
		bukkit.setExp(0);
		bukkit.giveExp(this.xp);
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public void addMoney(double money) {
		this.money += money;
	}

	public Player toBukkit() {
		return bukkit;
	}

	public MinigameData getMinigameData(String minigameName) {
		if (minigameData.containsKey(minigameName))
			return minigameData.get(minigameName);
		MinigameData md = new MinigameData(0, 0, 0, 0);
		minigameData.put(minigameName, md);
		return md;
	}

	public void sendMessage(String messageCode, Object... complements) {
		bukkit.sendMessage(LanguageMain.get(this, messageCode, complements));
	}

	public void load() {
		Connection conn = MainSource.get().getConnection();
		try {
			PreparedStatement stmt = conn
					.prepareStatement("SELECT id, xp, money, language, name FROM hub_users WHERE uuid=?");
			stmt.setString(1, bukkit.getUniqueId().toString());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				this.dbId = rs.getInt("id");
				this.xp = rs.getInt("xp");
				this.money = rs.getDouble("money");
				this.lang = rs.getString("language");
				this.firstLogin = false;
				return;
			}
			stmt.close();
			stmt = conn
					.prepareStatement("INSERT INTO hub_users (uuid, name, xp, money, language) VALUES (?, ?, ?, ?, ?)");
			stmt.setString(1, bukkit.getUniqueId().toString());
			stmt.setString(2, bukkit.getName());
			stmt.setInt(3, 0);
			stmt.setInt(4, 0);
			stmt.setString(5, "en_UK");
			stmt.execute();
			stmt.close();
			rs = conn.createStatement().executeQuery("SELECT LAST_INSERT_ID()");
			rs.first();
			this.dbId = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void loadMinigamesScore() {
		Connection conn = MainSource.get().getConnection();
		try {
			PreparedStatement stmt = conn.prepareStatement(
					"SELECT hub_minigame_stats.minigame, hub_minigame_stats.kills, hub_minigame_stats.deaths, hub_minigame_stats.played, hub_minigame_stats.wins "
							+ "FROM hub_minigame_stats INNER JOIN hub_users ON hub_users.id = hub_minigame_stats.player_id WHERE hub_users.uuid=?");
			stmt.setString(1, bukkit.getUniqueId().toString());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				minigameData.put(rs.getString("hub_minigame_stats.minigame"),
						new MinigameData(rs.getInt("hub_minigame_stats.kills"), rs.getInt("hub_minigame_stats.deaths"),
								rs.getInt("hub_minigame_stats.played"), rs.getInt("hub_minigame_stats.wins")));
			}
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void save() {
		Connection conn = MainSource.get().getConnection();
		try {
			PreparedStatement stmt = conn
					.prepareStatement("UPDATE hub_users SET xp=?, money=?, language=?, name=? WHERE id=?");
			stmt.setInt(1, xp);
			stmt.setDouble(2, money);
			stmt.setString(3, lang);
			stmt.setString(4, bukkit.getName());
			stmt.setInt(5, dbId);
			stmt.execute();
			for (Entry<String, MinigameData> entry : minigameData.entrySet()) {
				stmt = conn.prepareStatement(
						"SELECT hub_minigame_stats.id FROM hub_minigame_stats INNER JOIN hub_users ON hub_users.id = hub_minigame_stats.player_id WHERE hub_users.uuid=? AND hub_minigame_stats.minigame=?");
				stmt.setString(1, toBukkit().getUniqueId().toString());
				stmt.setString(2, entry.getKey());
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					stmt = conn.prepareStatement(
							"UPDATE hub_minigame_stats SET kills=?, deaths=?, played=?, wins=? WHERE id=?");
					stmt.setInt(1, entry.getValue().getKills());
					stmt.setInt(2, entry.getValue().getDeaths());
					stmt.setInt(3, entry.getValue().getPlayed());
					stmt.setInt(4, entry.getValue().getWins());
					stmt.setInt(5, rs.getInt("hub_minigame_stats.id"));
					stmt.execute();
					return;
				}
				stmt.close();
				stmt = conn.prepareStatement(
						"INSERT INTO hub_minigame_stats (player_id, minigame, kills, deaths, played, wins) VALUES (?, ?, ?, ?, ?, ?)");
				stmt.setInt(1, dbId);
				stmt.setString(2, entry.getKey());
				stmt.setInt(3, entry.getValue().getKills());
				stmt.setInt(4, entry.getValue().getDeaths());
				stmt.setInt(5, entry.getValue().getPlayed());
				stmt.setInt(6, entry.getValue().getWins());
				stmt.execute();
				stmt.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public class MinigameData {

		private int kills;
		private int deaths;
		private int played;
		private int wins;

		/**
		 * @param kills
		 *            Player kills
		 * @param deaths
		 *            Player deaths
		 * @param played
		 *            Played games
		 * @param wins
		 *            Wins
		 * @param xp
		 *            Total xp on game
		 * @author Rexcantor64
		 */
		public MinigameData(int kills, int deaths, int played, int wins) {
			this.kills = kills;
			this.deaths = deaths;
			this.played = played;
			this.wins = wins;
		}

		public int getKills() {
			return kills;
		}

		public int getDeaths() {
			return deaths;
		}

		public int getPlayed() {
			return played;
		}

		public int getWins() {
			return wins;
		}

		public void setKills(int kills) {
			this.kills = kills;
		}

		public void addKills(int kills) {
			this.kills += kills;
		}

		public void setDeaths(int deaths) {
			this.deaths = deaths;
		}

		public void addDeaths(int deaths) {
			this.deaths += deaths;
		}

		public void setPlayed(int played) {
			this.played = played;
		}

		public void addPlayed(int played) {
			this.played += played;
		}

		public void setWins(int wins) {
			this.wins = wins;
		}

		public void addWins(int wins) {
			this.wins += wins;
		}

	}

}
