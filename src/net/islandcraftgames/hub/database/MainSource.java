package net.islandcraftgames.hub.database;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class MainSource {

	private static MainSource instance = null;
	private MysqlDataSource dataSource;
	private Connection conn = null;

	public MainSource() {
		instance = this;
		dataSource = new MysqlDataSource();
		dataSource.setUser("mainplugins");
		dataSource.setPassword("zMpLaq92BQuWWyLT");
		dataSource.setServerName("test.islandcraftgames.net");
		dataSource.setDatabaseName("mainplugins");
	}
	
	public Connection getConnection(){
		if(conn == null)
			try {
				conn = dataSource.getConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		try {
			if(conn.isClosed()){
				dataSource = new MysqlDataSource();
				dataSource.setUser("mainplugins");
				dataSource.setPassword("zMpLaq92BQuWWyLT");
				dataSource.setServerName("test.islandcraftgames.net");
				dataSource.setDatabaseName("mainplugins");
				conn = dataSource.getConnection();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public static MainSource get(){
		if(instance == null)
			instance = new MainSource();
		return instance;
	}
}
