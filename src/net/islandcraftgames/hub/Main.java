package net.islandcraftgames.hub;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;

import net.islandcraftgames.api.LocationUtils;
import net.islandcraftgames.api.gui.GuiManager;
import net.islandcraftgames.api.minigames.GameManager;
import net.islandcraftgames.api.minigames.KitManager;
import net.islandcraftgames.api.minigames.SchematicManager;
import net.islandcraftgames.api.minigames.SignManager;
import net.islandcraftgames.api.minigames.VillagersManager;
import net.islandcraftgames.api.minigames.WorldGenerator;
import net.islandcraftgames.api.minigames.WorldManager;
import net.islandcraftgames.hub.commands.GamemodeCMD;
import net.islandcraftgames.hub.commands.HubCMD;
import net.islandcraftgames.hub.commands.LeaveCMD;
import net.islandcraftgames.hub.commands.SpectateCMD;
import net.islandcraftgames.hub.commands.StartCMD;
import net.islandcraftgames.hub.hotbar.HotbarMain;
import net.islandcraftgames.hub.listeners.PlayerListeners;
import net.islandcraftgames.hub.player.PlayerData;
import net.islandcraftgames.hub.tasks.TimerTask;
import net.islandcraftgames.minigames.listeners.BlockListener;
import net.islandcraftgames.minigames.listeners.PlayerListener;
import net.islandcraftgames.minigames.skywars.SkyWars;

public class Main extends JavaPlugin {

	private static Main instance;
	private SchematicManager schemManager;
	private SignManager signManager;
	private GuiManager guiManager;
	private GameManager gameManager;
	private KitManager kitManager;
	private VillagersManager villagersManager;
	private Location hub;
	private Location skyLobby;

	@Override
	public void onEnable() {
		instance = this;
		loadConfigValues();
		this.schemManager = new SchematicManager();
		this.schemManager.searchFolder(getDataFolder(), null);
		this.signManager = new SignManager();
		this.gameManager = new GameManager();
		this.kitManager = new KitManager();
		registerAllEvents();
		registerAllCommands();
		for (Player p : Bukkit.getOnlinePlayers())
			PlayerData.registerPlayer(p);
		Bukkit.getScheduler().runTaskLater(this, new Runnable() {
			@Override
			public void run() {
				registerEvents(villagersManager = new VillagersManager());
				loadMinigames();
				WorldManager.get().checkWorld();
			}
		}, 10L);
		new TimerTask().runTaskTimer(this, 0, 20L);
	}

	private void loadConfigValues() {
		saveResource("config.yml", false);
		hub = LocationUtils.fromString(getConfig().getString("locations.hub", "world;0;64;0;0;0"));
		skyLobby = LocationUtils.fromString(getConfig().getString("locations.sky", "world;1000;64;1000;0;0"));
	}

	private void loadMinigames() {
		new SkyWars();
	}

	private void registerAllEvents() {
		registerEvents(guiManager = new GuiManager());
		registerEvents(new PlayerListeners());
		registerEvents(new HotbarMain());
		registerEvents(new BlockListener());
		registerEvents(new PlayerListener());;
	}

	private void registerEvents(Listener l) {
		Bukkit.getPluginManager().registerEvents(l, this);
	}

	private void registerAllCommands() {
		regCmd("hub", new HubCMD());
		regCmd("leave", new LeaveCMD());
		regCmd("spectate", new SpectateCMD());
		regCmd("start", new StartCMD());
		regCmd("gamemode", new GamemodeCMD());
	}

	private void regCmd(String cmd, CommandExecutor executor) {
		getCommand(cmd).setExecutor(executor);
	}

	@Override
	public void onDisable() {
		this.villagersManager.unload();
		for (Player p : Bukkit.getOnlinePlayers())
			PlayerData.unregisterPlayer(p);
	}

	public static Main get() {
		return instance;
	}

	/**
	 * Gets the SchematicManager instance.
	 * 
	 * @author Rexcantor64
	 * @return The SchematicManager instance.
	 */
	public SchematicManager getSchematicManager() {
		return this.schemManager;
	}

	/**
	 * Gets the SignManager instance.
	 * 
	 * @author Rexcantor64
	 * @return The SignManager instance.
	 */
	public SignManager getSignManager() {
		return this.signManager;
	}

	/**
	 * Gets the GuiManager instance.
	 * 
	 * @author Rexcantor64
	 * @return The GuiManager instance.
	 */
	public GuiManager getGuiManager() {
		return this.guiManager;
	}

	/**
	 * Gets the GameManager instance.
	 * 
	 * @author Rexcantor64
	 * @return The GameManager instance.
	 */
	public GameManager getGameManager() {
		return this.gameManager;
	}

	/**
	 * Gets the KitManager instance.
	 * 
	 * @author Rexcantor64
	 * @return The KitManager instance.
	 */
	public KitManager getKitManager() {
		return this.kitManager;
	}

	/**
	 * Gets the VillagersManager instance.
	 * 
	 * @author Rexcantor64
	 * @return The VillagersManager instance.
	 */
	public VillagersManager getVillagersManager() {
		return this.villagersManager;
	}

	/**
	 * Gets the Hub Location.
	 * 
	 * @author Rexcantor64
	 * @return The Hub Location.
	 */
	public Location getHubLocation() {
		return this.hub;
	}

	/**
	 * Gets the SkyLobby Location.
	 * 
	 * @author Rexcantor64
	 * @return The SkyLobby Location.
	 */
	public Location getSkyLobbyLocation() {
		return this.skyLobby;
	}

	/**
	 * Gets the WorldEditPlugin instance.
	 * 
	 * @author Rexcantor64
	 * @throws RuntimeException
	 *             when WorldEdit is not found.
	 * @return The WorldEditPlugin instance.
	 */
	public WorldEditPlugin getWorldEdit() {
		Plugin we = Bukkit.getPluginManager().getPlugin("WorldEdit");
		if (we == null)
			throw new RuntimeException("WorldEdit is not loaded!");
		return (WorldEditPlugin) we;
	}

	@Override
	public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
		return new WorldGenerator();
	}

}
