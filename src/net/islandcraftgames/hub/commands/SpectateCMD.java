package net.islandcraftgames.hub.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.islandcraftgames.hub.player.IslandPlayer;
import net.islandcraftgames.hub.player.PlayerData;

public class SpectateCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		if (!(s instanceof Player)) {
			s.sendMessage("Only Players.");
			return true;
		}
		Player p = (Player) s;
		IslandPlayer ip = PlayerData.getData(p);
		if (args.length == 0) {
			if (!ip.isSpectating()) {
				ip.sendMessage("game.spectate.error", label);
				return true;
			}
			ip.getGame().removeSpectator(ip);
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			ip.sendMessage("cmd.playernotfound", args[0]);
			return true;
		}
		IslandPlayer targetip = PlayerData.getData(target);
		if (targetip.isPlaying()) {
			targetip.getGame().addSpectator(ip);
			p.setSpectatorTarget(target);
			ip.sendMessage("cmd.spectate.success", target.getName());
			return true;
		}
		if (targetip.isSpectating()) {
			targetip.getGame().addSpectator(ip);
			p.teleport(target);
			ip.sendMessage("cmd.spectate.success", target.getName());
			return true;
		}
		ip.sendMessage("cmd.playernotplaying", target.getName());
		return true;
	}

}
