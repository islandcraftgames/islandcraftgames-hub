package net.islandcraftgames.hub.commands;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.islandcraftgames.api.StringUtils;
import net.islandcraftgames.api.language.LanguageMain;

public class GamemodeCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		if (!(s instanceof Player)) {
			s.sendMessage("Only players!");
			return true;
		}
		Player p = (Player) s;

		if (!p.hasPermission("admin.gamemode"))
			return true;

		if (args.length == 0) {
			switch (p.getGameMode()) {
			case CREATIVE:
				p.setGameMode(GameMode.SURVIVAL);
				break;
			default:
				p.setGameMode(GameMode.CREATIVE);
				break;
			}
			p.sendMessage(LanguageMain.get(p, "cmd.gamemode.switch", p.getGameMode().name().toLowerCase()));
		} else {
			if (StringUtils.equalsIgnoreCase(args[0], "survival", "s", "0"))
				p.setGameMode(GameMode.SURVIVAL);
			else if (StringUtils.equalsIgnoreCase(args[0], "creative", "c", "1"))
				p.setGameMode(GameMode.CREATIVE);
			else if (StringUtils.equalsIgnoreCase(args[0], "adventure", "a", "2"))
				p.setGameMode(GameMode.ADVENTURE);
			else if (StringUtils.equalsIgnoreCase(args[0], "spectator", "sp", "spec", "3"))
				p.setGameMode(GameMode.SPECTATOR);
			else {
				p.sendMessage(LanguageMain.get(p, "cmd.gamemode.notfound", args[0]));
				return true;
			}
			p.sendMessage(LanguageMain.get(p, "cmd.gamemode.change", p.getGameMode().name().toLowerCase()));
		}
		return true;
	}
}
