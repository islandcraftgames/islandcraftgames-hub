package net.islandcraftgames.hub.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.islandcraftgames.api.minigames.events.GameForceStartEvent;
import net.islandcraftgames.hub.player.IslandPlayer;
import net.islandcraftgames.hub.player.PlayerData;

public class StartCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		if (!(s instanceof Player)) {
			s.sendMessage("Only Players.");
			return true;
		}
		Player p = (Player) s;
		IslandPlayer ip = PlayerData.getData(p);
		if (args.length == 0) {
			if (!ip.isPlaying()) {
				ip.sendMessage("game.notplaying");
				return true;
			}
			GameForceStartEvent e = new GameForceStartEvent(ip.getGame(), ip);
			Bukkit.getPluginManager().callEvent(e);
			if (e.isCancelled()) {
				ip.sendMessage("cmd.start.error", e.getErrorMessage());
				return true;
			}
			ip.sendMessage("cmd.start.success");
			e.getGame().startGame();
			return true;
		}
		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			ip.sendMessage("cmd.spectate.playernotfound", args[0]);
			return true;
		}
		IslandPlayer targetip = PlayerData.getData(target);
		if (targetip.isPlaying() || targetip.isSpectating()) {
			GameForceStartEvent e = new GameForceStartEvent(targetip.getGame(), ip);
			Bukkit.getPluginManager().callEvent(e);
			if (e.isCancelled()) {
				ip.sendMessage("cmd.start.error", e.getErrorMessage());
				return true;
			}
			ip.sendMessage("cmd.start.success.others", target.getName());
			e.getGame().startGame();
			return true;
		}
		ip.sendMessage("cmd.spectate.playernotplaying", target.getName());
		return true;
	}

}
