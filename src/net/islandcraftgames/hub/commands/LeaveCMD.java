package net.islandcraftgames.hub.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.islandcraftgames.hub.player.IslandPlayer;
import net.islandcraftgames.hub.player.PlayerData;

public class LeaveCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		if (!(s instanceof Player)) {
			s.sendMessage("Only Players.");
			return true;
		}
		Player p = (Player) s;
		IslandPlayer ip = PlayerData.getData(p);
		if (!ip.isPlaying()) {
			ip.sendMessage("game.notplaying");
			return true;
		}
		ip.getGame().removePlayer(ip);
		return true;
	}

}
