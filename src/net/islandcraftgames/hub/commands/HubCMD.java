package net.islandcraftgames.hub.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.islandcraftgames.hub.Main;

public class HubCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		//TODO Add messages
		if (s instanceof Player)
			((Player) s).teleport(Main.get().getHubLocation());
		return true;
	}

}
